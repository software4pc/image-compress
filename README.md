基于JavaFX的桌面程序。一个批量图片压缩软件，可设置缩放、压缩率

JDK version >= 8

[下载文件](https://gitlab.com/software4pc/image-compress/-/blob/master/classes/artifacts/ImageCompress/ImageCompress.jar) 双击运行

![软件截图](./软件截图.png)
